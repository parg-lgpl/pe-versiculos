/*
 * Projeto - PE Versículos
 * Este projeto é open source na licença LGPL, portanto é livre
 * para qualquer pessoa ou entidade usar e alterar.
 * 
 * Este projeto foi iniciado pensando na obra de Deus e como
 * um meio de demonstrar meu amor por Jesus.
 * 
 * @author Pablo Alexander da Rocha Gonçalves
 * @version 0.1
 * @since 13/05/2016
 * @link https://pargprogramador.wordpress.com/
 */
package br.com.parg.fx;

import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * Classe para configurar um objeto Scene como um SplashScreen
 * 
 * Para funcionar a raiz do formulário tem que ser um ArchorPane
 * configurado o seguinte CSS: -fx-background-color: transparent;
 * 
 * @author Pablo
 */
public class FXSplashScreen extends Scene {
    private final Stage stage;
    
    /**
     * Cria e configura um SplashScreen
     * @param root
     * @param stage 
     */
    public FXSplashScreen(Parent root, Stage stage) {
        super(root);
        this.stage = stage;
        // configura a janela
        stage.initStyle(StageStyle.TRANSPARENT);
        stage.setAlwaysOnTop(true);
        stage.setIconified(false);
        stage.setResizable(false);
        stage.setScene(this);
        
        // define a cor como transparent
        setFill(Color.TRANSPARENT);
    }

    /**
     * Retorna o Stage
     * @return 
     */
    public Stage getStage() {
        return stage;
    }
    
}
